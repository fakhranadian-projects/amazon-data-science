"""
        author: fakhran adian
"""

from application import (
                            ModelTrainer, TestData
)

import argparse

if __name__ == "__main__":

    parser      = argparse.ArgumentParser()

    # Pass an argument to choose whether training or testing the model
    parser.add_argument('--action', help="'train' / 'test'", type=str)
    
    args        = parser.parse_args()

    action      = args.action


    # Give option after running main.py
    if not action:
        
        while True:
            
            action      = input("'train' or 'test'?\n")

            if not action:

                print('Please enter an argument!')
                
            else:
                break
    

    # Give handling if argument passed not train or test
    if action not in ['train', 'test']:
        
        raise ValueError('Invalid action: should be train or test')
    
    
    if action == 'train':

        print('Training Algorithm')

        trainer = ModelTrainer()

        trainer.train()

        print('Training completed')


    elif action == 'test':

        test    = TestData(text='very bad game')

        test.test_data()