# about project
In the development of this machine learning, i have done several EDA and data cleaning. I also have train and tune the machine learning model resulting the best algorithm is SVM with kernel='linear' tuning with 95% accuracy and a good fit model.

In this code, i simply just do a clean code from development in jupyter notebook into several steps below:
- Import data from aws-rds database.
- Data preprocessing, cleaning, and tokenization.
- Polarity score to determine the scoring of text from vaderSentiment.
- Categorize the score into sentiment, where > 1 is Positive, = 0 is Neutral, and < 1 is Negative sentiment scores.
- Using TfidfVectorizer to make it readable for machine learning model, and matrices it using scipy.sparse.vstack.
- Model train with SVM algorithm and saved to local folder, then we can test the model from text in main.py to give result containing the texts, and positive, negative or neutral sentiment that saved within json format.