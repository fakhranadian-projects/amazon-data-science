from nltk.corpus import stopwords #type: ignore
from nltk.tokenize import TweetTokenizer #type: ignore
from sklearn.feature_extraction.text import TfidfVectorizer #type: ignore
from sklearn.model_selection import train_test_split #type: ignore
from vaderSentiment.vaderSentiment import SentimentIntensityAnalyzer #type: ignore
from sklearn.metrics import accuracy_score #type: ignore
from sklearn.svm import SVC #type: ignore
import pandas as pd #type: ignore
import re, string, pickle #type: ignore
import scipy.sparse as sp #type: ignore
from datetime import datetime as dt #type: ignore
from .conn import ConnectionDatabase


class TrainingData:

    def data_cleaning(self, text):

        if text is None:
            return '@'

        text = re.sub(r'http\S+|www\S+|https\S+', '', text)  # Remove URLs
        
        text = re.sub(r'[^a-zA-Z\s]', '', text)  # Remove non-alphabetic characters

        text = re.sub(r'<.*?>', '', text)  # Remove HTML tags
        
        return text
    

    def data_tokenize(self, text):

        # Define the list of stopwords
        new_stopwords = ['ive', 'til', 'till', 'ewww', 'theyr', 'theyre', 'html', 'ill',
                        'www', 'exe', 'winuae', 'cdrom', 'isnt', 'via', 'youre',
                        'fps', 'must', 'saw', 'fritz', 'psp', 'didnt', 'wouldnt',
                        'dont', 'neednt', 'mightnt', 'didnt', 'cant', 'havent', 'hasnt',
                        'shouldnt', 'just', 'ago', 'titlesjust', 'Name', 'dtype', 'youll',
                        'wasnt', 'given', 'arent', 'would', 'couldnt', 'hadnt', 'wont'
                    ]

        # get english stopwords from nltk.words
        stop_words = stopwords.words('english')

        # append and set new stopwords
        stop_words.extend(new_stopwords)

        stop_words = set(stop_words)


        # Converts all words to lowercase, Removes Twitter handles (tagname) from the text
        # Reduces repeated characters in a word to a maximum of three characters. For example, "coooooool" becomes "coool".

        tokenizer = TweetTokenizer(preserve_case=False, strip_handles=True, reduce_len=True)

        data_tokens = tokenizer.tokenize(str(text)) 
        
        
        # Loop every words and take every words that is not in stop_words, not a punctuation (e.g. ".!"), and have length > 2
        # Append every word that match the condition and then join them again to a full review text

        clean_data = []

        for word in data_tokens:

            if (word not in stop_words 
                
                and word not in string.punctuation 
                
                and len(word) > 2):
                
                clean_data.append(word)

        # join the words again to make a sentence after cleaned and tokenized
        clean_text = " ".join(clean_data)
        
        return clean_text
    

    def data_preprocessing(self, text):
        
        train = TrainingData()

        text = train.data_cleaning(text)

        text = train.data_tokenize(text)
        
        return text 
    

    def read_data(self):

        # define connection to database
        con_database = ConnectionDatabase.conn_aws(self)


        # read data from aws-rds
        df_review = pd.read_sql("SELECT review FROM amazon_review_dm", con=con_database)


        # apply data_preprocessing function above
        train = TrainingData()
        df_review['review'] = df_review['review'].apply(train.data_preprocessing)


        # save the review that have been preprocessing into database for top_words_api
        df_review.to_sql('sentiment_review', con=con_database, if_exists='replace', index=False)

        
        # define Lexicon based sentiment analyzer
        analyzer = SentimentIntensityAnalyzer()


        # apply polarity_scores to compound score the review from vaderSentiment lexicon based
        scores  = [analyzer.polarity_scores(x) for x in df_review['review']]

        df_review['Compound_Score'] = [x['compound'] for x in scores]


        # Label the compound score into negative, neutral, and positive
        df_review.loc[df_review['Compound_Score'] < 0, 'Sentiment']   = 'Negative'

        df_review.loc[df_review['Compound_Score'] == 0, 'Sentiment']  = 'Neutral'

        df_review.loc[df_review['Compound_Score'] > 0, 'Sentiment']   = 'Positive'


        # labeling data manually
        convert = {'Negative': 0, 'Positive': 1, 'Neutral': 2}

        df_review['Sentiment'] = df_review['Sentiment'].map(convert)


        # vectorize the data into a scipy.sparse.vstack to convert review text into numeric
        # so the model can be fitted (vstack works like to_array())

        vectorizer = TfidfVectorizer()

        X = vectorizer.fit_transform(df_review['review'])

        X = sp.vstack(X)


        # target selection
        y = df_review['Sentiment']


        # split data
        X_train, X_test, y_train, y_test    = train_test_split(X, y, test_size=0.2, random_state=0)


        # train the best model algorithm from development
        model       = SVC(kernel='linear').fit(X_train, y_train)

        pred_svm    = model.predict(X_test)

        accuracy    = round(accuracy_score(y_test, pred_svm) * 100)


        # Generate model name with timestamp and accuracy
        model_name  = dt.now().strftime('%Y%m%d%H%M%S')


        # create model name and absolute path classifier svm
        model_clf           = f'/home/linux/handson/ds/amazon-data-science-local/model/application/model/clf/{model_name}_svm_{accuracy}.clf'

        model_clf_api       = f'/home/linux/handson/ds/amazon-data-science-local/api/api_forecast/model/clf/{model_name}_svm_{accuracy}.clf'     


        # create model name and absolute path vectorizer tfidf
        model_vector        = f'/home/linux/handson/ds/amazon-data-science-local/model/application/model/vector/{model_name}.vct'

        model_vector_api    = f'/home/linux/handson/ds/amazon-data-science-local/api/api_forecast/model/vector/{model_name}.vct'


        # save model svm
        if model_clf is not None:

            with open(model_clf, 'wb') as f:
                pickle.dump(model, f)


        if model_clf_api is not None:

            with open(model_clf_api, 'wb') as f:
                pickle.dump(model, f)



        # save model vector
        if model_vector is not None:
            
            with open(model_vector, 'wb') as f:
                pickle.dump(vectorizer, f)
                

        if model_vector_api is not None:
            
            with open(model_vector_api, 'wb') as f:
                pickle.dump(vectorizer, f)


        

