import os #type: ignore
from sqlalchemy import create_engine #type: ignore
from dotenv import load_dotenv #type: ignore

class ConnectionDatabase:

    def conn_aws(self):
        
        load_dotenv()

        URL = os.environ['urlaws']
        USR = os.environ['usraws']
        PAS = os.environ['pasaws']
        POR = os.environ['poraws']
        DB  = os.environ['dbaws']


        engine_one    = create_engine("postgresql://{USER}:{PASS}@{URL}:{PORT}/{DB}".format(
                                    URL=URL,
                                    USER=USR,
                                    PASS=PAS,
                                    DB=DB,
                                    PORT=POR
                                    ))
        return engine_one
