"""
        author: fakhran adian
"""

from datetime import datetime as dt
from.train import TrainingData


class ModelTrainer:

    def train(self):

        beg_dt          = dt.now()
       
        self.mlmodel = TrainingData.read_data(self)
       
        print(f'Training completed in {round((dt.now() - beg_dt).total_seconds(), 3)} s')