import pandas as pd #type: ignore
import pickle, os, glob #type: ignore
from datetime import datetime as dt #type: ignore


class TestData:

    def __init__(self, text):
        
        self.text       = text

    
    @staticmethod
    def load_model(model_path):

        with open(model_path, 'rb') as f:

            return pickle.load(f)
        
    
    def test_data(self):

        # Define model paths
        path_clf = './application/model/clf/'
        
        path_vct = './application/model/vector/'


        # Use glob to find all files in the specified path
        files_clf = glob.glob(path_clf + '*')

        files_vct = glob.glob(path_vct + '*')
        

        # Sort the files based on their accuracy and modification time 
        files_clf.sort(key=lambda x: (int(x[-6:-4]), x), reverse=True)

        files_vct.sort(reverse=True)


        # Model handling if not exists
        if not files_clf:

            return print('Classifier models not found')

        if not files_vct:

            return print('Vector models not found')
            

        # Get the latest file path
        latest_file_clf = files_clf[0]

        latest_file_vct = files_vct[0]


        # Load classifier and vector models
        model_clf = self.load_model(latest_file_clf)

        model_vector = self.load_model(latest_file_vct)


        # Read new text from main.py
        new_text = str(self.text)
        
        # Vectorize the new_text
        new_vector = model_vector.transform([new_text]).toarray()

        # Predict the sentiment
        y_preds = model_clf.predict(new_vector)
        
        # Get the current timestamp
        current_timestamp = dt.now().strftime("%Y%m%d%H%M%S")
        

        if y_preds == 1:

            details     = [{'sentiment':
                           
                            {'text': new_text,
                            'results': 'positive'},

                            'ts': current_timestamp                            
                        }]
            
            
            # Save metadata to local folder
            predict     = pd.DataFrame(details)

            filename    = 'predict_positive_{}.json'.format(current_timestamp)

            file_path = os.path.join('./metadata/', filename)

            predict.to_json(file_path, orient='records', lines=True)

            print('Positive Sentiment')


        elif y_preds == 0:

            details     = [{'sentiment':
                           
                            {'text': new_text,
                            'results': 'negative'},

                            'ts': current_timestamp                            
                        }]


            # Save metadata to local folder
            predict     = pd.DataFrame(details, index=['1'])

            filename    = 'predict_negative_{}.json'.format(current_timestamp)
            
            file_path = os.path.join('./metadata/', filename)

            predict.to_json(file_path, orient='records', lines=True)

            print('Negative Sentiment')


        else:

            details     = [{'sentiment':
                           
                            {'text': new_text,
                            'results': 'neutral'},

                            'ts': current_timestamp                            
                        }]


            # Save metadata to local folder
            predict     = pd.DataFrame(details, index=['1'])

            filename    = 'predict_neutral_{}.json'.format(current_timestamp)

            file_path = os.path.join('./metadata/', filename)

            predict.to_json(file_path, orient='records', lines=True)

            print('Neutral Sentiment')