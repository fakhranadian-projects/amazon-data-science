"""
    author: fakhran adian
"""

from fastapi import FastAPI
from api_forecast.controllers import api_router


def create_app():

    app = FastAPI()

    app.include_router(api_router)

    return app