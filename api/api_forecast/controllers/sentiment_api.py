"""
    author: fakhran adian
"""

import pickle, json, enchant, os, glob, pandas as pd #type: ignore
from fastapi import APIRouter #type: ignore
from collections import Counter #type: ignore
from datetime import datetime as dt #type: ignore
from .conn import ConnectionDatabase

# Define router
router: APIRouter = APIRouter()


def load_model(model_path):

    with open(model_path, 'rb') as f:

        return pickle.load(f)


# Correct spelling automatically
def spell_correct(text: str) -> str:

    # Define spell checker
    spell_checker = enchant.Dict("en_US")

    
    # In spell_checker there's few word suggestion, but the closest one to a typo should be suggest index 0
    corrected_words = [spell_checker.suggest(word)[0] if not spell_checker.check(word) else word for word in text.split()]


    # Join back the words
    corrected_text = " ".join(corrected_words)


    return corrected_text



# Predict new text sentiment
@router.post('/sentiment-analysis')
def predictive_sentiment(review_text: str = None, correct_spelling: bool = True):
    
    # Get the current timestamp
    current_timestamp = dt.now().strftime("%Y%m%d%H%M%S") 


    if review_text is None:

        no_response =  [{'sentiment': 
                    
                        {'text': 'no review given',
                        'result': 'neutral'},
                        
                        'ep': '/sentiment-analysis',
                        'ts': current_timestamp
                        }]
        
        
        # Return response and save json format into local metadata
        metadata = pd.DataFrame(no_response)

        filename    = f'predict_neutral_{current_timestamp}.json'

        file_path = os.path.join('/metadata/sentiment-analysis/', filename)

        metadata.to_json(file_path, orient='records', lines=True)

        return no_response
    

    # define model path
    path_clf = './api_forecast/model/clf/'

    path_vct = './api_forecast/model/vector/'


    # Use glob to find all files in the specified path
    files_clf = glob.glob(path_clf + '*')

    files_vct = glob.glob(path_vct + '*')


    # Sort the files based on their accuracy and modification time (index is fixed because the format _svm_95.clf)
    files_clf.sort(key=lambda x: (int(x[-6:-4]), x), reverse=True)

    files_vct.sort(reverse=True)


    # Get the latest file path
    latest_file_clf = files_clf[0]

    latest_file_vct = files_vct[0]


    # load model clf and vector
    model_clf   = load_model(latest_file_clf)

    model_vector = load_model(latest_file_vct)


    # Perform spelling correction if requested
    if correct_spelling:

        corrected_text = spell_correct(review_text.lower())
        
    else:

        corrected_text = review_text.lower()


    # Lower the text to give the top-words non-duplicated text with uppercase
    text = f'{corrected_text.lower()}'


    # vectorize text
    vector = model_vector.transform([text]).toarray()


    # predict text sentiment
    y_preds = model_clf.predict(vector)


    # Define predict result
    if y_preds == 1:

        response = [{'sentiment': {

                    'text': text,
                    'result': 'positive'
        },

        'ep': '/sentiment-analysis',
        'ts': current_timestamp
        }]


        # Return response and save json format into local metadata
        metadata = pd.DataFrame(response)

        filename    = f'predict_positive_{current_timestamp}.json'

        file_path = os.path.join('/metadata/sentiment-analysis/', filename)

        metadata.to_json(file_path, orient='records', lines=True)

    
    elif y_preds == 0:
        
        response = [{'sentiment': {

                    'text': text,
                    'result': 'negative'
        },
        
        'ep': '/sentiment-analysis',
        'ts': current_timestamp
        }]


        # Return response and save json format into local metadata
        metadata = pd.DataFrame(response)

        filename    = f'predict_negative_{current_timestamp}.json'

        file_path = os.path.join('/metadata/sentiment-analysis/', filename)

        metadata.to_json(file_path, orient='records', lines=True)


    else:

        response = [{'sentiment': {

                    'text': text,
                    'result': 'neutral'
        },

        'ep': '/sentiment-analysis',
        'ts': current_timestamp
        }]


        # Return response and save json format into local metadata
        metadata = pd.DataFrame(response)

        filename    = f'predict_neutral_{current_timestamp}.json'

        file_path = os.path.join('/metadata/sentiment-analysis/', filename)

        metadata.to_json(file_path, orient='records', lines=True)
        
    return response



@router.get('/sentiment-top-words')
def top_10_words():

    # Get the current timestamp
    current_timestamp = dt.now().strftime("%Y%m%d%H%M%S") 


    # Initialize an empty list to store sentiment texts
    sentiment_text = []


    # Use glob to get the file paths of all metadata files in the folder
    path = glob.glob('/metadata/sentiment-analysis/' + '*')


    # Iterate over the metadata files
    for file_name in path:

        with open(file_name, 'r') as file:
            metadata = json.load(file)


        # Extract the sentiment text from the metadata
        sentiment = metadata.get('sentiment')

    
        if sentiment and 'text' in sentiment:
            sentiment_text.append(sentiment['text'])


    # Define connection to database
    conn_aws = ConnectionDatabase.conn_aws()
    

    # Execute a SQL query to retrieve sentiment texts
    with conn_aws.connect() as connection:
        result = connection.execute("SELECT review FROM sentiment_review")


        # Iterate over the result set and append sentiment texts to the list
        for row in result:
            sentiment_text.append(row[0])


    # Close the database connection
    conn_aws.dispose()


    # Concatenate all sentiment texts into a single string
    all_text = ' '.join(sentiment_text)


    # Convert the text to lowercase
    lower_text = all_text.lower()


    # Tokenize the lowercase text into words
    words = lower_text.split()


    # Count the frequency of each word
    word_counts = Counter(words)


    # Get the top 10 most common words
    top_words = word_counts.most_common(10)


    # Create a list of word-frequency pairs
    top_words_list = [{'word': word, 'frequency': count} for word, count in top_words]


    response        = [{'sentiment':{
                        
                        'top_words': top_words_list},
                        
                        'ep': '/sentiment-top-words',
                        'ts': current_timestamp
                        }]
    
    
    # Return response and save json format into local metadata
    metadata = pd.DataFrame(response)

    filename    = f'sentiment_top_words_{current_timestamp}.json'

    file_path = os.path.join('/metadata/sentiment-top-words/', filename)

    metadata.to_json(file_path, orient='records', lines=True)

    return response







