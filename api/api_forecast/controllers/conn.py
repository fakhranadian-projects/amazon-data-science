import os, boto3 #type: ignore
from sqlalchemy import create_engine #type: ignore
from dotenv import load_dotenv #type: ignore

class ConnectionDatabase:

    def conn_aws():
        
        load_dotenv()

        URL = os.environ['urlaws']
        USR = os.environ['usraws']
        PAS = os.environ['pasaws']
        POR = os.environ['poraws']
        DB  = os.environ['dbaws']

        engine_one    = create_engine("postgresql://{USER}:{PASS}@{URL}:{PORT}/{DB}".format(
                                    URL=URL,
                                    USER=USR,
                                    PASS=PAS,
                                    DB=DB,
                                    PORT=POR
                                    ))
        
        return engine_one
    
    
    def conn_s3():

        # load dotenv
        load_dotenv()

        KEY_ID          = os.environ['key']
        ACCESS_KEY      = os.environ['acc']
        REGION          = os.environ['reg']

        # create endpoint
        endpoint        = boto3.client(
                            's3',
                            aws_access_key_id       = KEY_ID,
                            aws_secret_access_key   = ACCESS_KEY,
                            region_name             = REGION
                        )
        
        return endpoint