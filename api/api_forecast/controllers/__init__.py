"""
    author: fakhran adian
"""

from fastapi import APIRouter #type: ignore
from api_forecast.controllers import sentiment_api, model_api #type: ignore

api_router  = APIRouter()

api_router.include_router(sentiment_api.router, tags=['Amazon Sentiment Analysis'])

api_router.include_router(model_api.router, tags=['Amazon NLP Model'])



