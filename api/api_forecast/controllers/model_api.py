"""
    author: fakhran adian
"""

import glob, pickle, os, pandas as pd #type: ignore
from fastapi import APIRouter #type: ignore
from datetime import datetime as dt #type: ignore
from . conn import ConnectionDatabase


# Define router
router: APIRouter = APIRouter()


# AWS S3 bucket and folder information
bucket_name = 'bucket-january-fakhran'
folder_name = 'model/'


def load_model(model_path):

    with open(model_path, 'rb') as f:

        return pickle.load(f)
    


@router.get('/latest-model')
def highest_accuracy_model():

    # Get the current timestamp
    current_timestamp = dt.now().strftime("%Y%m%d%H%M%S")


    # Get model path
    path  = './api_forecast/model/clf/'


    # Use glob to find all files in the specified path
    files = glob.glob(path + '*')


    # Sort the files based on their accuracy and modification time
    files.sort(key=lambda x: (int(x[-6:-4]), x), reverse=True)


    # Get the latest file path
    latest_file = files[0]


    # Save file name
    file_name = os.path.basename(latest_file)


    # Get the accuracy of the model
    accuracy = int(file_name[-6:-4])


    # return a response
    response = [{'latest_model':
                 
                {'local_file_path': latest_file,
                 's3_bucket_name': bucket_name,
                 's3_file_path': f'{folder_name}{file_name}',
                 'model_accuracy': round(accuracy/100, 2)},
                
                 'ep': '/latest-model',
                 'ts': current_timestamp
                 }]
    

    # Return response and save json format into local metadata
    metadata = pd.DataFrame(response)

    filename    = f'latest_model_{current_timestamp}.json'

    file_path = os.path.join('/metadata/latest-model/', filename)

    metadata.to_json(file_path, orient='records', lines=True)
    
    return response
    


@router.put('/upload-sentiment-model')
def upload_latest_model_to_s3_bucket():

    # Get the current timestamp
    current_timestamp = dt.now().strftime("%Y%m%d%H%M%S")


    # Initialize AWS S3 client
    s3_client = ConnectionDatabase.conn_s3()


    # Model path
    model_folder_path = "./api_forecast/model/clf/"


    # Use glob to find all files in the specified path
    files = glob.glob(model_folder_path + '*')


    # Sort the files based on their modification time
    files.sort(reverse=True)


    # Get the latest file path
    latest_file = files[0]
    

    # Get the latest file name
    latest_file_name = os.path.basename(latest_file)


    # Construct the destination key with the "model" folder
    destination_key = folder_name + latest_file_name


    # Extract datetime and accuracy from the file name
    accuracy = int(latest_file_name[-6:-4])
    

    # Check if the file already exists in the bucket
    check_model = s3_client.list_objects_v2(Bucket=bucket_name, Prefix=destination_key)


    if 'Contents' in check_model:

        model_exists = [{'upload_model':
                     
                        {"msg": f'model {latest_file_name} already exists'},

                        'ep': '/upload-sentiment-model',
                        'ts': current_timestamp
                        }]
                        
                        
        # Return response and save json format into local metadata
        metadata = pd.DataFrame(model_exists)

        filename    = f'upload_sentiment_model_{current_timestamp}.json'

        file_path = os.path.join('/metadata/upload-sentiment-model/', filename)

        metadata.to_json(file_path, orient='records', lines=True)

        return model_exists
    

    # Upload the file to S3
    s3_client.upload_file(latest_file, bucket_name, destination_key)

    response = [{'upload_model':
                 
                {"msg": f"model {latest_file_name} uploaded successfully",
                 'model_accuracy': round(accuracy/100, 2)},
                 
                 'ep': '/upload-sentiment-model',
                 'ts': current_timestamp
                 }] 
    
    
    # Return response and save json format into local metadata
    metadata = pd.DataFrame(response)

    filename    = f'upload_sentiment_model_{current_timestamp}.json'

    file_path = os.path.join('/metadata/upload-sentiment-model/', filename)

    metadata.to_json(file_path, orient='records', lines=True)

    return response
    



@router.delete('/delete-sentiment-model')
def delete_models_except_highest_accuracy(key: str = None):

    # Get the current timestamp
    current_timestamp = dt.now().strftime("%Y%m%d%H%M%S")


    # Check if the key is valid
    if key is None:

        not_author = [{'delete_models': 
                       {'msg': 'no key were given, please insert required key'},

                        'ep': '/delete-sentiment-model',
                        'ts': current_timestamp
                        }]
        
        
        # Return response and save json format into local metadata
        metadata = pd.DataFrame(not_author)

        filename    = f'unauthorized_{current_timestamp}.json'

        file_path = os.path.join('/metadata/delete-sentiment-model/', filename)

        metadata.to_json(file_path, orient='records', lines=True)

        return not_author
    

    # Check if the key is valid
    elif key != "fakhranadian":

        not_author = [{'delete_models': 
                       {'msg': 'unauthorized user, please contact admin'},

                        'ep': '/delete-sentiment-model',
                        'ts': current_timestamp
                        }]
        
        
        # Return response and save json format into local metadata
        metadata = pd.DataFrame(not_author)

        filename    = f'unauthorized_{current_timestamp}.json'

        file_path = os.path.join('/metadata/delete-sentiment-model/', filename)

        metadata.to_json(file_path, orient='records', lines=True)

        return not_author
    
    
    # Define model clf and vector path
    clf_path = './api_forecast/model/clf/'

    vct_path = './api_forecast/model/vector/'


    # Use glob to find all files in the specified path
    files = glob.glob(clf_path + '*')


    # Sort the files based on their accuracy and modification time
    files.sort(key=lambda x: (int(x[-6:-4]), x), reverse=True)


    # Remove model that is not the highest accuracy and the latest model
    deleted_files = []
    

    # Remove clf file after sorting
    for clf_file in files[1:]:
        os.remove(clf_file)


        # Saved deleted model list
        deleted_files.append(clf_file)
        

        # Get the filename
        clf_filename = os.path.basename(clf_file)


        # Remove the _svm_accuracy.clf and replace with .vct so we can locate the same model name
        vct_filename = clf_filename.replace(clf_filename[-11:], '.vct')
        

        # Get the vector file path
        vct_file = os.path.join(vct_path, vct_filename)

        
        # Remove vct file with the same name as clf
        if os.path.exists(vct_file):
            os.remove(vct_file)
            

            # Shows every file that has been deleted
            deleted_files.append(vct_file)

    
    response = [{'delete_models': 
                {"msg": "models deleted successfully.",
                 "deleted_files": deleted_files},
                 
                 'ep': '/delete-sentiment-model',
                 'ts': current_timestamp
                 }]
    
    
    # Return response and save json format into local metadata
    metadata = pd.DataFrame(response)

    filename    = f'delete_sentiment_model_{current_timestamp}.json'

    file_path = os.path.join('/metadata/delete-sentiment-model/', filename)

    metadata.to_json(file_path, orient='records', lines=True)

    return response