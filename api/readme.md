# About Project
This project uses the model we trained before and generate an API for user to interact with. Since i run this within my local device, there might be a lot of directory you have to change to hit the endpoint, but other from that, it should be fine.

- To use the API, simply go to the directory of this folder, open terminal and type uvicorn main:app --reload --log-level debug. In this project i use nginx in my system daemon to prevent security breach.
- Temporary url can be achieve by typing this in the terminal: ngrok http 8000 --basic-auth='username:password'.
- Then we can just copy the url and add /docs by the end of the url to enter the API web.