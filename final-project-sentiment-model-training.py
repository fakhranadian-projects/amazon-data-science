from airflow.operators.bash_operator import BashOperator
from airflow import DAG
from datetime import datetime
import pendulum

local_tz = pendulum.timezone("Asia/Jakarta")

args = {'owner':'airflow'}

dag1 = DAG(
    'final-project-ds-training-model',
    start_date = datetime(2023, 5, 2, tzinfo=local_tz),
    schedule_interval = '0 2 2 * *',
    default_args = args
)

etl1 = BashOperator(
                task_id= 'final-project-training-model',
                bash_command= 'python3 /home/linux/handson/ds/FINAL-PROJECT/sentiment-model/main.py',
                dag=dag1
)

etl1
