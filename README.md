# Amazon Product and Reviews Data Science Projects
![FAST-API Endpoint](/API.PNG)

## Overview
This project is about Amazon Products and Reviews machine learning model and API, if you wanted to use this API endpoint, you'll need to perform the data engineer ETL first because we need the clean data from review data mart to perform machine learning model in this project.

I use VaderSentiment lexicon based sentiment analysis for scoring, and Support Vector Machine (SVM) algorithm in this project. You can access the vaderSentiment documentation within this link: [vaderSentiment](https://github.com/cjhutto/vaderSentiment)

Several tools and programming language used in this project:
- Python
- PostgreSQL
- FAST-API
- AWS S3 Bucket
- AWS RDS

## Machine Learning
- Data we use in this ml model is from amazon_review_dm in DE project, where we take the review column only.
- We'll define some function to make the data suitable for sentiment analysis process, that include cleaning stopwords, punctuation, tagline, etc.
- After doing some cleaning, we can apply vaderSentiment lexicon polarity_scores in [SentimentIntensityAnalyzer](https://github.com/cjhutto/vaderSentiment/blob/master/vaderSentiment/vaderSentiment.py) Class into the texts review.
- We'll then mark all the scores achieve from the polarity score into negative, neutral, and positive. All the score marks within score -1 up to 1, where < 1 is Negative, = 0 is Neutral, and > 1 is Positive. 
- In development, we can also check the distribution for this score, which in this case could probably different because of sampling in DE projects. In my case though, there's 82% Positive, 11% Negative, and 7% Neutral Sentiment. Seeing those distribution, we can assume that the negative and neutral sentiment might not be as accurate as positive because of how much data it has for training in this ml model.
- After that we can just label the sentiment manually by negative = 0, positive = 1, and neutral = 0.
- I use [TfidfVectorizer](https://scikit-learn.org/stable/modules/generated/sklearn.feature_extraction.text.TfidfVectorizer.html) to vectorize the text into number so it can be train, and [scipy.sparse.vstack](https://docs.scipy.org/doc/scipy/reference/generated/scipy.sparse.vstack.html) library to matrices the data.
- After several training, hyperparameter tuning, and cross validation on several supervised machine learning algorithm, i found that the best f1-score and accuracy achieve with Support Vector Machine(SVM) algorithm with kernel='linear' tuning.
- Finally we wanted to save the model to a folder and sent it into API folder as well so we can hit on API endpoint later.
- I also made a testing scenario and save the metadata in json format to check some sentiment locally.

## API Endpoint
In general, all endpoint had a same format where it always saved into json format locally and given timestamp and endpoint source everytime someone hit the API, you can just change the directory later in the code.

I made several endpoint that we can hit in this API as described in lists below.

### POST/sentiment-analysis
This endpoint is used to predict future texts sentiment, where we can just simply put some text and it retrieve a json format result containing sentiment and other detail. I made a feature in this endpoint:

- Correct Spelling: I made this feature to prevent typos from user, where it sometimes can be a crucial words such as bad, good, etc. It will automatically detect and return the first words suggestion from [enchant.Dict](https://pyenchant.github.io/pyenchant/tutorial.html). For example it can correct bda into bad so the sentiment won't be bias by a typo.

### GET/sentiment-top-words
This endpoint can be used to get top 10 words from all of our review texts, it also read from local metadata where everytime someone hit sentiment-analysis endpoint, it gave another text to be inserted into top words.

### GET/latest-model
I made this endpoint to retrieve which model are currently the have the highest accuracy and latest trained model, so we can always maintain the best model available. It return file path from local folder, and also path through s3 bucket where we saved our metadata.

### PUT/upload-sentiment-model
This endpoint used to sent model metadata into S3 bucket, so we can keep tracks of how our model progress.

### DELETE/delete-sentiment-model
This last endpoint is and exclusive one for the API creator, where we can delete every irrelevant model that had been saved locally to mantain cloud storage if we're using cloud server. I made a feature where it requires password/key to hit this endpoint. It will return a note or warning and saved it to local metadata folder if the one who hit this endpoint doesn't provide or incorrectly type the password.

### .env file key into database
In /model/application and api/api_forecast folder, there's .env file contains key for database access, you can change the key into your own database or just simply change it into local database e.g below:

```
#This is my username, password, and db name for my local postgresql database, you can change it into your own database
#localhost
urlaws = "localhost"
usraws = "postgres"
pasaws = "postgres"
poraws = "5432"
dbaws  = "postgres"

# secret key dan access key s3 bucket
key = 'YOUR_S3_SECRET_KEY'
acc = 'YOUR_S3_ACCESS_KEY'
reg = 'YOUR_S3_REGION'
```

## Contact
Please feel free to contact me if you have any questions. [Fakhran Adian](https://www.linkedin.com/in/fakhranadian/)
